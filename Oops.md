# Object-oriented programming(Oops)
Oops stands for object-oriented programming, in this kind of programming we consider all the thing as objects, to work with the real-life problem we use oops concept, there are few important concepts in oops which are object, class, inheritance, encapsulation etc.
>#### Object :-
>**Objects can be any real-world entity which has some properties and behavior, there are many examples of objects like car, dog, cat etc.**

>Objects can also be defined as the instance of a class. Every object consumes some memory and has some address.
>#### Class :-
>**Class is the blueprint of the object from which we can create as many objects of a kind as we want. It is a logical entity and it does not consume any space in the memory.**
>#### **There are four main pillars of Oops**
>1. Encapsulation
>1. Abstraction
>1. Inheritance
>1. Polymorphism

>#### **We will discuss each one of them separately** 

>#### 1. Encapsulation :-
>**In object-oriented programming we bind similar functions and variables in a single object, this is called encapsulation.**

>It makes our code more readable, increases the reusability and decreases the complexity of the code.

>#### 2. Abstraction :-
>**Hiding the non important data and displaying only the essential part with the user is called abstraction.**

>We can take the example of a car to understand the concept of abstraction, to start a car a user press the start key and car get started but what happened insight the car after pressing the start button it is not important for the user so we hide that information from the user which is not relevant for him, and this is called abstraction.

>#### 3. Inheritance :-
>**Inheritance is the property of object-oriented programming by virtue of which the child class can inherit or access the properties of its parent class.**

>This property of oops makes the code reusable.

>#### 4. Polymorphism
>**The word polymorphism is used in various contexts and describes situations in which something occurs in several different forms. In computer science, it describes the concept that objects of different types can be accessed through the same interface. Each type can provide its own, independent implementation of this interface. It is one of the core concepts of object-oriented programming (OOP).**

[JavatPoint:-Oops concept ]
(https://www.javatpoint.com/java-oops-concepts)